// index.js
import requestMixin from './request';

@reactMixin.decorate(requestMixin)
class Foo {
    // ...
    handleSaveClick = () => {
        this.get('http://...');
    }
}



// request.js
export const requestMixin = {
    get(url) {
        // ...
    }
}
