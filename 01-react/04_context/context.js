var Grandparent = React.createClass({
    childContextTypes: {
         message: React.PropTypes.string.isRequired
    },

    getChildContext: function() {
         return { message: "From Grandparent" };
    },

    render: function() {
         return <Parent />;
    }
});


var Parent = React.createClass({
    // Notice how this component does not have either the
    // getChildContext method or the childContextTypes map.
    render: function() {
        return <Child />;
    }
});

var Child = React.createClass({
    contextTypes: {
        message: React.PropTypes.string.isRequired
    },

    render: function() {
        return <div>message: {this.context.message}</div>;
    }
});
