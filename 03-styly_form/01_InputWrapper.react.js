import React, { Component } from 'react';
import { Input } from 'react-bootstrap';

class InputWrapper extends Component {

    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value
        };
    }

    // shouldComponentUpdate = shouldPureComponentUpdate;
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.value !== this.props.value || nextState.value !== this.state.value;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.acceptProps === true) {
            this.setState({
                value: nextProps.value
            });
        }
    }

    getValue() {
        return this.state.value;
    }

    handleChange = () => {
        this.setState({
            value: this.refs.input.getValue()
        }, function() {
            // console.log(this.state.value);
        });
    }

    render() {
        const label = this.props.label || null;
        const labelClassName = this.props.labelClassName || null;
        const type = this.props.type || 'text';
        const wrapperClassName = this.props.wrapperClassName || null;

        return (
            <Input
                bsStyle={this.props.bsStyle}
                type={type}
                ref='input'
                value={this.state.value}
                onChange={this.handleChange}
                label={label}
                labelClassName={labelClassName}
                wrapperClassName={wrapperClassName}
                help={this.props.help}
            />
        );
    }
}

InputWrapper.propTypes = {
    bsStyle: React.PropTypes.string,
    help: React.PropTypes.string,
    label: React.PropTypes.string,
    labelClassName: React.PropTypes.string,
    type: React.PropTypes.string,
    value: React.PropTypes.string,
    wrapperClassName: React.PropTypes.string
};

InputWrapper.defaultProps = {
    acceptProps: true,
    help: ''
};

export default InputWrapper;
