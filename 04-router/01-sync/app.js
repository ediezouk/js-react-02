import react from 'react';
import { history } from 'react-router/lib/HashHistory';
import { Router, Route } from 'react-router';

class App extends React.Component {
  render() {
    return (
        <div>
            <h1>Aplikace</h1>
            {this.props.children}
        </div>
    );
  }
}

class About extends React.Component {
  render() {
    return (
        <div>
            <h2>About</h2>
            <p>...</p>
        </div>
    );
  }
}

React.render((
  <Router history={history}>
    <Route path="/" component={App}>
      <Route path="about" component={About}/>
    </Route>
  </Router>
), document.getElementById('example'));
